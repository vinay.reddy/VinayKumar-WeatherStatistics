import React from 'react';
import './App.css'
import PlacesAutocomplete, { geocodeByAddress, getLatLng } from 'react-places-autocomplete'
class App extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        address: '' ,
        items: "",
        stateChange: false,
        temp: undefined,
        weather: "",
        relativehumidity: "",
        currentState: true,
        icon: "",
        forecast: "",
        lat:undefined,
        lng:undefined
      }
      this.onChange = (address) => this.setState({ address })
    }
    componentWillMount() {
      let scope = this
      fetch('http://api.wunderground.com/api/fdd94dbdbce6658a/conditions/q/CA/Bangalore.json')
        .then(reponse => reponse.json())
        .then(function (results) {
          scope.setState({
            temp: results.current_observation.temp_c,
            weather: results.current_observation.weather,
            relativehumidity: results.current_observation.relative_humidity,
            icon: results.current_observation.icon_url
          })
        })
      fetch('http://api.wunderground.com/api/fdd94dbdbce6658a/forecast/q/CA/Bangalore.json')
        .then(reponse => reponse.json())
        .then(function (results) {
          scope.setState({ forecast: results.forecast.simpleforecast.forecastday })
        })
    }
    weatherFetching() {
      
    }
    forecastWeather() {
      this.setState({ currentState: false })
    }
    currentWeather(){
      this.setState({ currentState: true })
    }
  handleFormSubmit = (event) => {
    event.preventDefault()
    let scope=this;
    geocodeByAddress(this.state.address)
      .then(results => getLatLng(results[0]))
      .then(latLng => {
        this.setState({lat:latLng.lat,lng:latLng.lng})
        console.log(this.state.lat,this.state.lng)
      }).then(()=>{
        console.log(this.state.lat)
        fetch('http://api.wunderground.com/api/fdd94dbdbce6658a/conditions/q/'+this.state.lat+','+this.state.lng+'.json')
      .then(reponse => reponse.json())
      .then(function (results) {
        console.log(results);
        scope.setState({
          temp: results.current_observation.temp_c,
          weather: results.current_observation.weather,
          relativehumidity: results.current_observation.relative_humidity,
          icon: results.current_observation.icon_url,
          currentState:true
        })
      })
      fetch('http://api.wunderground.com/api/fdd94dbdbce6658a/forecast/q/'+this.state.lat+','+this.state.lng+'.json')
      .then(reponse => reponse.json())
      .then(function (results) {
        scope.setState({ forecast: results.forecast.simpleforecast.forecastday })
      })
    })
      .catch(error => console.error('Error', error))
      
      console.log(this.state.address)
      
  }

  render() {
    const inputProps = {
      value: this.state.address,
      onChange: this.onChange,
    }
    const cssClasses = {
      root: 'form-group',
      input: 'textBox',
      autocompleteContainer: 'my-autocomplete-container'
    }
    var currentWeather = this.weatherStatus()
    return (
      <div className="container">
        <header>
          <div>
            <img src={require('./partly_cloudy.png')} alt="cloud" />
          </div>
          <div>
            <h6>Weatherforcast</h6>
          </div>
        </header>
        <div className="filteredStates">
          <div className="searchBar">
          <div>
            <PlacesAutocomplete classNames={cssClasses} inputProps={inputProps} />
            </div>
            <div>
            <input type="submit" value="search" className="searchButton" onClick={this.handleFormSubmit.bind(this)} />
            </div>
          </div>
        </div>
        <div className="weatherStatistics">
          <input type="submit" value="Currentforecast" className="todayStatistics" onClick={this.currentWeather.bind(this, this.state.address)} />
          <input type="submit" value="Futureforecast" className="futureStatistics" onClick={this.forecastWeather.bind(this, this.state.address)} />
        </div>
        <div className="div-forecastWeather" >
          {currentWeather}
        </div>
      </div>
    )
  }
  weatherStatus() {
    let currentWeather;
    if (this.state.currentState === true) {
      currentWeather = <div className="todayStatisticsDetails">
        <div className="weather">
          <img src={this.state.icon} alt="partly-cloudy" />
          Weather:{this.state.weather}
        </div>
        <div className="relativeHumidity">
          RelativeHumidity:{this.state.relativehumidity}
        </div>
        <div className="temperature">
          Temperature:{this.state.temp}
        </div>
      </div>
    }
    else {
      currentWeather = this.state.forecast.map((weather, index) => {
        if (index !== 0) {
          let date = weather.date.monthname + " " + weather.date.day + "," + weather.date.year;
          return (
            <div key={date} className="forecastWeather" >
              <div>{date}</div>
              <div>
                <div className="image-icon" >
                  <img src={weather.icon_url} alt="weather-icon" />
                  <span>{weather.conditions}</span>
                </div>
                <div>
                  <div><span>MaxTemperature:{weather.high.celsius}</span></div>
                  <div><span>MinTemperature:{weather.low.celsius}</span></div>
                </div>
                <div>Humidity:{weather.avehumidity}</div>
              </div>
            </div>
          )
        }
      })
    }
    return currentWeather;
  }
}
export default App