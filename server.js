var express = require('express');
var app = express();
const port = 5000;
const request = require("request");
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });
app.get('/weather', function (req, res) {
const url =
'http://autocomplete.wunderground.com/aq?query='+req.query['state'];
request.get(url, (error, response, body) => {
  let json = JSON.parse(body);
  res.send(json)
});
})
  app.listen(port, function () {
    console.log('Magic happens on port ' + port);
  });